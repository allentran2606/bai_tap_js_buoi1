/*
******** Bài 1: Tính lương nhân viên ********

Input: Lương 1 ngày, số ngày làm (user nhập)
    + Test case số ngày: 28, 30 ,56, 1, 20000, 90, 78



Step: Tổng lương = Lương 1 ngày * số ngày làm



Output: Tổng lương

*/

// Bài 1
var luongNgay = 100000;
var soNgay = 28;
var tongLuong = null;
console.log(`Tổng lương nhận:`, (tongLuong = luongNgay * soNgay));

/*
 ******** Bài 2: Tính giá trị trung bình ********


Input: 5 số thực
    + 1, 2, 3, 4, 5
    + 5, 90, 34, 78, 21
    + 8549, 389214, 3428, 8999999, 54
    + 82, 912118947869, 438, 1900384, 348
    + 438, 1984, 1894, 89, 71



Step: tính tổng 5 số chia 5



Output: Giá trị trung bình của 5 số

 */

//Bài 2

var a = 8549,
  b = 389124,
  c = 3428,
  d = 8999999,
  e = 54;
var trungBinh = null;

trungBinh = (a + b + c + d + e) / 5;
console.log(`Giá trị trung bình là: ${trungBinh}`);

/*
 ******** Bài 3: Quy đổi tiền ********

 Input: Giá 1 USD (23500), số tiền USD (user nhập)
    + soTien: 90, 48, 98219382938, 1548, 84933, 890141



 Step: VND = Giá 1 USD * số tiền USD



 Output: VND
 */

//  Bài 3

var usd = 23500;
var soTien = 1548;
var vnd = null;
console.log(`Số tiền VND: ${(vnd = usd * soTien)}`);

/*
 ******** Bài 4: Tính diện tích và chu vi HCN ********

 Input: chiều dài chiều rộng
    + 10 và 67, 87 và 57, 8493 và 49398


 Step: diện tích = dài * rộng
       chu vi = (dai + rộng) * 2


Output: diện tích và chu vi
 */

// Bài 4
var a = 8493,
  b = 49398;
var dienTich,
  chuVi = null;
dienTich = a * b;
chuVi = (a + b) * 2;
console.log(`Diện tích là: ${dienTich}        `, `Chu vi là: ${chuVi}`);

/*
 ******** Bài 5: Tính diện tích và chu vi HCN ********

 Input: Số có 2 ký số
    + 12, 44, 83, 98, 69, 76


Step: Tách phần đơn vị: num % 10
      Tách phần chục: num / 10
      Cộng đơn vị và chục


Output: Tổng
 */

// Bài 5
var n = 98;
var tong = null;
var donVi,
  hangChuc = Number;
donVi = n % 10;
hangChuc = Math.floor(n / 10);
tong = donVi + hangChuc;
console.log(`Tổng là: ${tong}`);
